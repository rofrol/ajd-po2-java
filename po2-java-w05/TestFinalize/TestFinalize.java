public class TestFinalize {

  public static void main(String[] args) {
    System.out.println("Ilość utworzonych obiektów typu Klasa = " + Klasa.ile);
    Klasa k1 = new Klasa();
    System.out.println("Ilość utworzonych obiektów typu Klasa = " + k1.ile);
    Klasa k2 = new Klasa();
    System.out.println("Ilość utworzonych obiektów typu Klasa = " + Klasa.ile);
    k1 = k2 = null;
    //uruchomi sie kiedy bedzie chcial, dlatego finalize moze nie byc uruchomiona
    //w trakcie wykonywania programu, szczegolnie przy pierwszym zaladowaniu javy
    //do pamieci
    System.gc(); // wywołanie odśmiecacza pamięci

    System.out.println("Ilość aktualnie istniejących obiektów typu Klasa = " + Klasa.ile);
  }
}

class Klasa {
  
  Klasa() {
    ++ile;
  }

  protected void finalize() {
    --ile;
  }

  public static int ile = 0;
}

