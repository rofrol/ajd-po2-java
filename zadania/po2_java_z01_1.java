//po2-java-z01-1

import pl.czest.ajd.imi.frolow.*;
import static java.lang.Math.*;

class po2_java_z01_1
{
    public static void main(String argsp[])
    {
        System.out.println("Zadanie: wczytaj n liczb rzeczywistych, podaj wyniki dla podpunktów.");
        System.out.println("\nNajpierw podaj ile liczb wczytać.");
        int n = InputValidation.podajInt();

        System.out.println("\nTeraz podaj te liczby.");
        float a = 0, b = 1, c = 0, d = 0, e = 1, f = 0, h = 0, i = 0;
        float j [] = new float [n];
        float wej = 0;
        for (int k = 1; k <= n; ++k)
        {
            wej = InputValidation.podajFloat();
            a += wej;
            b *= wej;
            c += abs(wej);
            d += sqrt(abs(wej));
            e *= abs(wej);
            f += wej * wej;
            h += pow(-1, k+1) * wej;
            i += pow(-1, k) * wej / Factorial.factorial(k);
            j[k-1] = wej;
        }
        System.out.println();
        System.out.println("a) " + a);
        System.out.println("b) " + b);
        System.out.println("c) " + c);
        System.out.println("d) " + d);
        System.out.println("e) " + e);
        System.out.println("f) " + f);
        System.out.println("h) " + h);
        System.out.println("i) " + i);
        for (int k = 1; k <= n; ++k)
        {
        System.out.print(j[n-k] + ", ");
        }
    }
}
