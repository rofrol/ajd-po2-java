//po2-java-z01-2.1.h

import pl.czest.ajd.imi.frolow.InputValidation;

class po2_java_z01_2_1_h
{
    public static void main(String argsp[])
    {
        System.out.println("Zadanie: wczytaj n liczb, podaj ile liczb jest takich ze, |a[k]| < k*k.");
        System.out.println("\nNajpierw podaj ile liczb wczytać.");
        int n = InputValidation.podaj_liczbe();

        System.out.println("\nTeraz podaj te liczby.");
        int a = 0;
        int ile = 0;
        for (int k = 1; k <= n; ++k)
        {
            a = InputValidation.podaj_liczbe();
            if (a > -k*k && a < k*k) ++ile;
        }
        System.out.println("\nile = " + ile);
    }
}
