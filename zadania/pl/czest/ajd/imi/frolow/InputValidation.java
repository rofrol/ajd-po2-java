package pl.czest.ajd.imi.frolow;
import java.util.InputMismatchException;
import java.util.Scanner;

public class InputValidation
{
    public static int podajInt()
    {
        Scanner keyboard = new Scanner(System.in);
        int number = 0;
        boolean numberIsValid = false;
        // Data validation loop
        while (numberIsValid == false)
        {
            System.out.print("Please enter your number: ");
            try
            {
                number = keyboard.nextInt();
                numberIsValid = true;
            }
            catch (InputMismatchException ex)
            {
                System.out.println("Invalid input.  Please try again.");
                // IMPORTANT: Remove the garbage from the input stream
                keyboard.nextLine();
            }
        }
        return number;
    }
    public static float podajFloat()
    {
        Scanner keyboard = new Scanner(System.in);
        float number = 0;
        boolean numberIsValid = false;
        // Data validation loop
        while (numberIsValid == false)
        {
            System.out.print("Please enter your number: ");
            try
            {
                number = keyboard.nextFloat();
                numberIsValid = true;
            }
            catch (InputMismatchException ex)
            {
                System.out.println("Invalid input.  Please try again.");
                // IMPORTANT: Remove the garbage from the input stream
                keyboard.nextLine();
            }
        }
        return number;
    }
}
