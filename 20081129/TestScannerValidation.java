import java.util.Scanner;
import java.util.InputMismatchException;

//http://academic2.strose.edu/math_and_science/goldschd/spring2008-nys-java-week02-ex.html
// Example14 showing how to read console input in
// from the user via the keyboard using Scanner
// (with error checking)

public class TestScannerValidation {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        // Input the user's name
        System.out.print("Please enter your name: ");
        String name = keyboard.next();  // or keyboard.nextLine();

        System.out.println("Hello, " + name);

        int age = 0;
        boolean ageIsValid = false;

        // Data validation loop
        while (ageIsValid == false) {
            System.out.print("Please enter your age: ");

            try {
                age = keyboard.nextInt();
                ageIsValid = true;
            } catch (InputMismatchException ex) {
                System.out.println("Invalid input.  Please try again.");
                // IMPORTANT: Remove the garbage from the input stream
                keyboard.nextLine();
            }

            if (ageIsValid) {
                // validate age range
                if (age < 10 || age > 120) {
                    System.out.println("Invalid age.  Please re-enter.");
                    ageIsValid = false;
                }
            }
        }

        age += 6;    // age = age + 6;

        System.out.println("In 6 years, you'll be " + age + " years old.");
    }
}
