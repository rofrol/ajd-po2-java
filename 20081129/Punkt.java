//2008-11-29
public class Punkt {
	public static void main(String[] args){
		Punkt a = new Punkt(2,5);
		a.show();
		a.move(3,5);
		a.show();
	}
	public Punkt(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX(){
		return x;
	}

	public int getY(){
		return x;
	}

	public void setX(int x){
		this.x = x;
	}

	public void setY(int y){
		this.y = y;
	}

	public void move(int deltaX, int deltaY) {
		x = x + deltaX;
		y = y + deltaY;
	}

	public void show() {
		System.out.println("<" + getX() + "," + getY() + ">");
	}
	private int x;
	private int y;
}
