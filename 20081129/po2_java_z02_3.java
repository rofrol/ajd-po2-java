//po2-java-z02-3
//Iloczyn macierzy a i b.

//scanner
//http://beginwithjava.blogspot.com/2008/07/getting-keyboard-input-for-console-apps.html

import java.util.Scanner;

class po2_java_z02_3
{
    public static void main(String argsp[])
    {
        int m = podaj_liczbe();
        System.out.println("m = " + m);
        int n = podaj_liczbe();
        System.out.println("n = " + n);
        int k = podaj_liczbe();
        System.out.println("k = " + k);
        System.out.println();        

        int [][] a;
        a = new int[m][n];
        int [][] b;
        b = new int[n][k];
        int [][] c;
        c = new int[m][k];
        
        System.out.println("Macierz a");
        for(int i = 0; i < m; ++i)
        {
            for(int j = 0; j < n; ++j)
            {
                a[i][j] = i + j;
                System.out.print(a[i][j] + ",");
            }          
            System.out.println();
        }


        System.out.println("\nMacierz b");
        for(int i = 0; i < n; ++i)
        {
            for(int j = 0; j < k; ++j)
            {
                b[i][j] = i + j;
                System.out.print(b[i][j] + ",");
            }          
            System.out.println();
        }

        System.out.println("\nMacierz c");
        for(int i = 0; i < m; ++i)
        {
            for(int j = 0; j < k; ++j)
            {
                for(int s = 0; s < n; ++s)
                {
                        c[i][j] += a[i][s] * b[s][j];
                }
                System.out.print(c[i][j] + ",");
            }
            System.out.println();
        }


    }

    public static int podaj_liczbe()
    {    
        int zmienna = 0;
        Scanner input = new Scanner(System.in);
        System.out.println("Podaj liczbe od 1 do 10.");
        
        while (input.hasNextInt())
        {
            zmienna = input.nextInt();
            if (zmienna < 1 || zmienna > 10)
            {
                System.out.println("Podaj liczbe od 1 do 10.");
            }
            else
            {
                break;
            }
        }
        return zmienna;
    }
}