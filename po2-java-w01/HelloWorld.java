/**
 *
 * Klasa HelloWorld implementuje aplikację
 * wyświetlajacą na ekranie napis
 * Hello, world!
*/ 

public class HelloWorld {
  public static void main(String[] args) {
    System.out.println("Hello, world!");
  }
}

