// Na podstawie książki Cay Horstmann, Gary Cornell. 
// "Java (TM). Podstawy." Wydanie VIII"

import java.util.*;

class Pracownik {
    public Pracownik (String nazwisko, double pobory, int year, int month, int day) {
        this.nazwisko = nazwisko;
        this.pobory = pobory;

        // klasa GregorianCalendar numeruje miesiące począwszy od 0
        GregorianCalendar calendar = new GregorianCalendar (year, month - 1, day);

        dataZatrudnienia = calendar.getTime();
        // dataZatrudnienia = new Date(year, month, day); metoda niezalecana

        id = nextId;
        ++nextId;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public double getPobory() {
        return pobory;
    }

    public Date getDataZatrudnienia() {
        return (Date) dataZatrudnienia.clone();
        // niebezpieczna byłaby instrukcja: return dataZatrudnienia;
    }

    public int id() {
        return id;
    }

    public void setId() {
        id = nextId;
        ++nextId;
    }

    public static int getNextId() {
        return nextId;
    }

    public void zwiekszPobory (double procent) {
        double podwyżka = pobory * procent / 100;
        pobory += podwyżka;
    }

    private String nazwisko;
    private double pobory;
    private Date dataZatrudnienia;

    private int id;
    private static int nextId = 1;
}

