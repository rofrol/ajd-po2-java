// Na podstawie książki Cay Horstmann, Gary Cornell. 
// "Java (TM). Podstawy." Wydanie VIII"

import java.util.*;

public class TestArrayList {

    public static void main (String[] args) {
        ArrayList<Pracownik> personel = new ArrayList<Pracownik>();

        personel.add (new Pracownik ("Carl Cracker", 75000, 1987, 12, 15));
        personel.add (new Pracownik ("Harry Hacker", 50000, 1989, 10, 1));
        personel.add (new Pracownik ("Tony Tester", 40000, 1990, 3, 15));

        for (Pracownik e : personel) {
            e.zwiekszPobory (5);
        }

        for (Pracownik e : personel) {
            System.out.println ("Nazwisko = " + e.getNazwisko()
                                + ", pobory = " + e.getPobory()
                                + ", data Zatrudnienia = " + e.getDataZatrudnienia()
                               );
        }

        System.out.println ("\nUsuwamy pracownika o indeksie 1\n");
        Pracownik p = personel.remove(1);

        for (Pracownik e : personel) {
            System.out.println("Nazwisko = " + e.getNazwisko()
                                + ", pobory = " + e.getPobory()
                                + ", data Zatrudnienia = " + e.getDataZatrudnienia()
                               );
        }

        Object[] kopia = personel.toArray();

        Pracownik[] tab = new Pracownik[personel.size()];
        personel.toArray(tab);

    }
}

