public class Test03 {
  
  public static void main(String[] args) {
    String[] names = new String[3];
    names[0] = "Andrzej";
    names[1] = "Robert";
    names[2] = "Tomasz";
    Person ja = new Person(names);
    System.out.println(ja.getName());
    Person nobody = new Person(null);
  }

}

class Person {
  
  public Person(String[] names) {
    assert names != null;
    this.names = new String[names.length];
    for (int j = 0; j < names.length; ++j) {
      this.names[j] = names[j];
    }
  }

  public String getName() {
    StringBuffer buf = new StringBuffer("");
    for (int j = 0; j < names.length; ++j) {
      buf.append(names[j]).append(" ");
    }
    return new String(buf);
  }

  private String[] names;
}

