import java.util.*;

public class TestSmplArrayList {

    public static void main (String[] args) {
        ArrayList<Integer> a = new ArrayList<Integer>();

        a.ensureCapacity (20);
        for (int k = 0; k < 20; ++k) {
            a.add (k * k);
        }

        System.out.println(a.size());
        for (int k = 0; k < 20; ++k) {
            System.out.print(a.get (k) + " ");
        }
        System.out.println();

        Integer b[] = new Integer[a.size()];
        a.toArray (b);

        System.out.println(b.length);
        for (int k = 0; k < 20; ++k) {
            System.out.print (b[k] + " ");
        }
        System.out.println();
    }

}

