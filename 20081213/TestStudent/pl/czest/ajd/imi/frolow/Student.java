package pl.czest.ajd.imi.frolow;
import static java.lang.System.*;

import java.util.*;

//utworzenie dokumentacji za pomoca nazwy pakietu
//javadoc -d doc pl.czest.ajd.imi.frolow

/**
klasa student
*/
public class Student {
    /**konstruktor dla nazwiska i daty urodzenia*/
    public Student(String nazwisko, int year, int month, int day) {
        this.nazwisko = nazwisko;
        GregorianCalendar kalendarz = new GregorianCalendar(year, month - 1, day);
        dataUrodzenia = kalendarz.getTime();
        ++ile;
    }

    /**konstruktor dla sredniej, ktory dodatkowo uruchamia konstruktor dla nazwiska
    i daty urodzenia*/
    public Student(String nazwisko, double srednia, int year, int month, int day) {
        this(nazwisko, year, month - 1, day);

        this.srednia = srednia;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public double getSrednia() {
        return srednia;
    }

    public void setSrednia(double srednia) {
        this.srednia = srednia;
    }

    public Date getDataUr() {
        return dataUrodzenia;
    }

    protected void finalize() {
        --ile;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return ("nazwisko: " + getNazwisko() + ", data urodzenia: " + getDataUr() + ", srednia ocen: " + getSrednia() + ", ile: " + Student.ile + ", Id: " + getId());

    }

    private String nazwisko;
    private double srednia;
    private Date dataUrodzenia;
    public static int ile = 0;


    private int id;
    private static int nastId;

    // blok inicjalizacyjny dla pól statycznych
    static {
        Random generator = new Random();
        // ustawia statyczne pole nastId na pseudolosowa wartosc
        // z przedzialu od 0 do 9999
        nastId = generator.nextInt (10000);
    }

    // blok inicjalizacyjny dla obiektów
    {
        id = nastId;
        ++nastId;
    }

}

//setNazwisko i setSrednia
//komentarz przed klasa, metoda, pole i javadoc uzyc