// klasa Pracownik zdefinowana w pakiecie pl.czest.ajd.cs.zbrzezny
import pl.czest.ajd.imi.frolow.*;

import static java.lang.System.*;

public class TestStudent {

    public static void main(String[] args) {
        Student joe = new Student("Joe Programmer", 4.5, 1989, 10, 1);

        out.println("nazwisko: " + joe.getNazwisko() + ", data urodzenia: " + joe.getDataUr() + ", srednia ocen: " + joe.getSrednia() + ", ile: " + Student.ile + ", Id: " + joe.getId());

        Student jim = new Student("Jim Designer", 4.3, 1989, 11, 13);
        out.println("nazwisko: " + jim.getNazwisko() + ", data urodzenia: " + jim.getDataUr() + ", srednia ocen: " + jim.getSrednia() + ", ile: " + Student.ile + ", Id: " + jim.getId());

        joe.setNazwisko("Joe Niszczyciel");
        joe.setSrednia(3.2);
        out.println("nazwisko: " + joe.getNazwisko() + ", data urodzenia: " + joe.getDataUr() + ", srednia ocen: " + joe.getSrednia() + ", ile: " + Student.ile + ", Id: " + joe.getId());

        joe.setNazwisko("Joe Unbeatable");
        //automatyczne wywolane metody toString, wystarczy ja zaimplementowac w klasie
        out.println(joe);
    }
}

//nazwisko
//data uro
//srednia ocen